import React from "react";
//importing components

import './App.css';
import {Switch, Route} from 'react-router-dom';
//import {useParams} from 'react-router';
import Nav from './components/Nav';
import About from './components/About';
import Shop from './components/Shop';
import Details from './components/Details';



const App = () =>{
   // let {id} = useParams;
    // const Child = () =>{
    //     return {
    //         Id: {id}
    //     }
    // }

    return(
        <>
         <Nav/>
        <div className = "App">
            
        <Route path = "/about"><About/></Route>
        <Route path = "/shop" exact><Shop/></Route>
        <Route path = "/shop/:id"><Details/></Route>
        </div>
       
        </>
    )
        
} 

export default App;