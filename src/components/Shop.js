import React,{useState, useEffect} from 'react'
import {Link} from 'react-router-dom';

function Shop() {

    useEffect(() => {
        fetechItems();
    },[])

    const [items,setItems] = useState([]);


    const fetechItems = async () =>{
        const response = await fetch('https://jsonplaceholder.typicode.com/posts');
        const objects = await response.json();
        //console.log(objects);
       setItems(objects);
    
    }
    // console.log("==================");
    // console.log(items);

    return (
        <div>
            {
                items.map((item) => (
                    // console.log(item.title)

                   <h3 key = {item.id}>
                       <Link to = {`/shop/${item.id}`}>
                       {item.title}
                       </Link>
                       
                       </h3>
                )
                //     console.log(item)
                //    console.log(item.userId)
                //    console.log(item.id)
                     
                )
            }

        </div>
    )
}

export default Shop
