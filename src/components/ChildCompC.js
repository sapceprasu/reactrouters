import React from 'react'
import {ElementArray} from './Nav' 
import {useContext} from 'react'


function ChildCompC() {

    const arrayContext = useContext(ElementArray);


    return (
        <div>
            <h1>Child component C.</h1><br/>
            <h1>Hi I am trying context API in react</h1>
               {/* <h1>{arrayContext}</h1>  */}
             {
                 arrayContext.map((item) =>{
                     
                          {return(
                            <ul> 
                                <li>
                                     {item}
                                </li>
                            </ul>      
                           
                              )
                                                        
                        }
                     
                    
                     
                 })
             }
        </div>
    )
}

export default ChildCompC
