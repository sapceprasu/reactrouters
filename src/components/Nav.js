import React from 'react';
import './Nav.css';
import {Link} from 'react-router-dom';
//importing compontents 

import ChildcompA from './ChildCompA';
import {createContext} from 'react';


const ElementArray = createContext();

// const elementArrayContent = [{email:'prasu@gmail.com'},{gender: 'Male'},{subject:'Engineering'}];
const elementArrayContent =['pranav','bhandari','swift','technology',1997]

function Nav() {
    return (
        <div>
            <nav>
                <h3>logo</h3>
                <ul className= "nav-links">
                    <Link to = '/about'>
                    <li>About</li>
                    </Link>
                    <Link to = '/shop'>
                    <li>Shop</li>
                    </Link>  
                </ul>
            </nav>
            <div className = 'childcomps'>
                <ElementArray.Provider value = {elementArrayContent}>
                     <ChildcompA/>
                </ElementArray.Provider>
               

            </div>

        </div>
    )
}

export default Nav;
export {ElementArray};

