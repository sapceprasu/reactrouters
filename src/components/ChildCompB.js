import React from 'react'
import ChildCompC from './ChildCompC'
//import {ElementArray} from './Nav'

function ChildCompB() {
    return (
        <div>
            <ChildCompC/>
            {/* <ElementArray.Consumer>
                {
                    (arrays)=>{
                        arrays.map((element)=>{
                            <h1>{console.log(element.email)}</h1>
                            
                        })
                    }
                    // <ChildCompC/>
                }
            </ElementArray.Consumer> */}
        </div>
    )
}

export default ChildCompB;

